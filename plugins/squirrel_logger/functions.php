<?php

/**
  * SquirrelMail Squirrel Logger Plugin
  *
  * Copyright (c) 2005-2011 Paul Lesniewski <paul@squirrelmail.org>
  * Copyright (c) 2002-2003 Pat Winn <ptwinn@velocibyte.com>
  * Copyright (c) 2001-2004 Ron Chinn <ron@squeaksoft.com>
  *
  * Licensed under the GNU GPL. For full terms see the file COPYING.
  *
  * @package plugins
  * @subpackage squirrel_logger
  *
  */


/**
  * Load configuration file
  *
  * @param boolean $silent When TRUE, does not display any errors, simply
  *                        returns FALSE (OPTIONAL; default = FALSE)
  *
  * @return boolean TRUE when the configuration file was loaded correctly,
  *                 FALSE when $quiet is turned on and there was an error
  *                 loading the configuration file (if there was an error
  *                 and $quiet is turned off, program execution will be
  *                 halted after an error is shown on-screen).
  *
  */
function sl_get_config($quiet=FALSE)
{

   // I don't think we should try to load the default/example
   // file; this plugin has too many system-specific and/or
   // sensitive aspects to try to make a misconfigured plugin work.
   //
   if (!@include_once(SM_PATH . 'config/config_squirrel_logger.php'))
      if (!@include_once(SM_PATH . 'plugins/squirrel_logger/config.php'))
      {
         if ($quiet)
            return FALSE;

         echo 'ERROR: can\'t load squirrel_logger config file';
         exit(1);
      }

   return TRUE;

}



/**   
  * Validate that this plugin is configured correctly
  *
  * @return boolean Whether or not there was a
  *                 configuration error for this plugin.
  *
  */
function squirrel_logger_check_configuration_do()
{

   global $sl_log_events, $sl_logs;


   if (!sl_get_config(TRUE))
   {
      do_err('Squirrel Logger plugin is missing its main configuration file', FALSE);
      return TRUE;
   }


   // check for questionable logger events
   //
   foreach ($sl_logs as $log => $allowed_events)
   {
   
      foreach ($allowed_events as $event => $message)
      {
         if (!in_array($event, $sl_log_events))
            do_err('Squirrel Logger plugin is configured to log the "' . $event . '" event in $sl_logs, but that event is not enabled in $sl_log_events', FALSE);
      }


      // make sure the log type is known
      //
      if ($log != 'FILE' && $log != 'SQL' && strpos($log, 'SYSTEM') !== 0)
      {
         do_err('Squirrel Logger plugin is configured to use an unknown log type "' . $log . '" in $sl_logs', FALSE);
         return TRUE;
      }

   }


   // only need to do this pre-1.5.2, as 1.5.2 will make this
   // check for us automatically
   //
   if (!check_sm_version(1, 5, 2))
   {

      // try to find Compatibility, and then that it is v2.0.10+
      //
      if (function_exists('check_plugin_version')
       && check_plugin_version('compatibility', 2, 0, 10, TRUE))
         return FALSE;


      // something went wrong
      //
      do_err('Squirrel Logger plugin requires the Compatibility plugin version 2.0.10+', FALSE);
      return TRUE;

   }

   return FALSE;

}



/**
  * Get date formatted in local or GMT time
  *
  * @param string $format Date format string
  * @param int $timestamp Time to be formatted
  *
  */
function sldate($format, $timestamp)
{

   global $sl_use_GMT;
   sl_get_config();

   if ($sl_use_GMT)
      return gmdate($format, $timestamp);
   else
      return date($format, $timestamp);
}



/**
  * Log user logins
  *
  */
function sl_log_login_do() 
{

   global $login_username, $sl_log_events, 
          $domain, $only_log_domains, $skip_domains;
   sl_get_config();


   if (!in_array('LOGIN', $sl_log_events) 
    || (isset($only_log_domains['LOGIN']) && is_array($only_log_domains['LOGIN']) 
        && !empty($only_log_domains['LOGIN']) && !in_array($domain, $only_log_domains['LOGIN']))
    || (isset($skip_domains['LOGIN']) && is_array($skip_domains['LOGIN']) 
        && !empty($skip_domains['LOGIN']) && in_array($domain, $skip_domains['LOGIN'])))
      return;


//TODO: is $login_username correct when called before vlogin decyphers it?
//TODO: is $login_username correct when called before login_alias changes it?
//TODO: is $login_username correct when called before password_forget changes it?
//TODO: what about $domain??
//            these seem to work OK at least as long as this plugin 
//            comes after those... haven't yet tried the converse
   sl_logit('LOGIN', '', $login_username);
//$log_str = "Successful webmail login from " . $ip_str . " by " . $login_username . ".";

}



/**
  * Log user logouts
  *
  */
function sl_log_logout_do() 
{

   global $sl_log_events,
          $domain, $only_log_domains, $skip_domains;
   sl_get_config();


   // check for timeout_user plugin - did user time out?
   //
   $event = 'LOGOUT';
   if (sqgetGlobalVar('timeout', $timeout, SQ_FORM)
    || sqgetGlobalVar('timeout_immediate', $timeout_immediate, SQ_FORM))
      $event = 'TIMEOUT';


   if (!in_array($event, $sl_log_events)
    || (isset($only_log_domains[$event]) && is_array($only_log_domains[$event])
        && !empty($only_log_domains[$event]) && !in_array($domain, $only_log_domains[$event]))
    || (isset($skip_domains[$event]) && is_array($skip_domains[$event])
        && !empty($skip_domains[$event]) && in_array($domain, $skip_domains[$event])))
      return;


   sl_logit($event);
//$log_str = "Webmail session timed out for " . $username . " from " . $ip_str . ".";
//$log_str = "Webmail user " . $username . " from " . $ip_str . " logged out.";

}



/**
  * Log user login errors
  *
  */
function sl_log_login_error_do($args) 
{

   global $login_username, $sl_log_events,
          $domain, $only_log_domains, $skip_domains;
   sl_get_config();


   if (!in_array('LOGIN_ERROR', $sl_log_events)
    || (isset($only_log_domains['LOGIN_ERROR']) && is_array($only_log_domains['LOGIN_ERROR'])
        && !empty($only_log_domains['LOGIN_ERROR']) && !in_array($domain, $only_log_domains['LOGIN_ERROR']))
    || (isset($skip_domains['LOGIN_ERROR']) && is_array($skip_domains['LOGIN_ERROR'])
        && !empty($skip_domains['LOGIN_ERROR']) && in_array($domain, $skip_domains['LOGIN_ERROR'])))
      return;


//TODO: is $login_username correct here when using vlogin, login_alias, or pwd_forget??
//            these seem to work OK at least as long as this plugin 
//            comes after those... haven't yet tried the converse
   $message = $args[1];
   sl_logit('LOGIN_ERROR', $message, $login_username);
//$log_str = "Failed webmail login from " . $ip_str . " by " . $login_username . ".";

}



/**
  * Log user errors
  *
  */
function sl_log_error_do($args) 
{

   global $sl_log_events, $reporting_error_for_squirrel_logger,
          $domain, $only_log_domains, $skip_domains;
   sl_get_config();


   if ($reporting_error_for_squirrel_logger
    || !in_array('ERROR', $sl_log_events)
    || (isset($only_log_domains['ERROR']) && is_array($only_log_domains['ERROR'])
        && !empty($only_log_domains['ERROR']) && !in_array($domain, $only_log_domains['ERROR']))
    || (isset($skip_domains['ERROR']) && is_array($skip_domains['ERROR'])
        && !empty($skip_domains['ERROR']) && in_array($domain, $skip_domains['ERROR'])))
      return;


   $message = $args;
   sl_logit('ERROR', $message);
//$log_str = "Failed webmail login from " . $ip_str . " by " . $login_username . ".";

}



/**
  * Monitor mails being sent, watching for possible mass mailings (spam)
  *
  */
function sl_monitor_present_msgs_do($args)
{

   $delimiter = '@';


   global $draft, $sl_mass_mail_limit, $domain,
          $sl_log_events, $only_log_domains, $skip_domains;


   // don't monitor drafts
   //
   if (!empty($draft)) return;


   sl_get_config();


   if (!in_array('MASS_MAILING', $sl_log_events)
    || (isset($only_log_domains['MASS_MAILING']) && is_array($only_log_domains['MASS_MAILING'])
        && !empty($only_log_domains['MASS_MAILING']) && !in_array($domain, $only_log_domains['MASS_MAILING']))
    || (isset($skip_domains['MASS_MAILING']) && is_array($skip_domains['MASS_MAILING'])
        && !empty($skip_domains['MASS_MAILING']) && in_array($domain, $skip_domains['MASS_MAILING'])))
      return;


   // grab TO, CC, and BCC fields from message
   //
   if (check_sm_version(1, 5, 2))
      $message = $args;
   else
      $message = $args[1];
   $toList = array();
   $ccList = array();
   $bccList = array();
   $replyToList = array();
   $fromList = array();

   foreach ($message->rfc822_header->to as $recip)
      $toList[] = $recip->mailbox . $delimiter . $recip->host;
   foreach ($message->rfc822_header->cc as $recip)
      $ccList[] = $recip->mailbox . $delimiter . $recip->host;
   foreach ($message->rfc822_header->bcc as $recip)
      $bccList[] = $recip->mailbox . $delimiter . $recip->host;
   foreach ($message->rfc822_header->reply_to as $sender)
      $replyToList[] = $sender->mailbox . $delimiter . $sender->host;
   foreach ($message->rfc822_header->from as $sender)
      $fromList[] = $sender->mailbox . $delimiter . $sender->host;


   // too many recipients?  log it and possibly send alert
   //
   $total_recips = sizeof($toList) + sizeof($ccList) + sizeof($bccList);
   if ($total_recips >= $sl_mass_mail_limit)
   {

      global $username, $sl_log_mass_mailing_show_recipients, 
             $sl_log_mass_mailing_show_message_body, $body,
             $sl_log_mass_mailing_show_subject,
             $sl_log_mass_mailing_show_from,
             $sl_log_mass_mailing_show_reply_to,
             $sl_dateformat, $sl_namelookups, $subject;
      $msg = 'Total ' . $total_recips . ' recipients';


      $address_delim = ', ';


      if ($sl_log_mass_mailing_show_recipients)
      {
         $msg .= ' (TO: ' . implode($address_delim, $toList) . ')';
         $msg .= ' (CC: ' . implode($address_delim, $ccList) . ')';
         $msg .= ' (BCC: ' . implode($address_delim, $bccList) . ')';
      }

      if ($sl_log_mass_mailing_show_from)
         $msg .= ' (FROM: ' . implode($address_delim, $fromList) . ')';

      if ($sl_log_mass_mailing_show_reply_to)
         $msg .= ' (REPLY-TO: ' . implode($address_delim, $replyToList) . ')';

      if ($sl_log_mass_mailing_show_subject)
         $msg .= ' (SUBJECT: ' . $subject . ')';

      if ($sl_log_mass_mailing_show_message_body)
         $msg .= ' (BODY: ' . $body . ')';


      sl_logit('MASS_MAILING', $msg);

   }

}



/**
  * Monitor mails when they are sent
  *
  */
function sl_monitor_sent_msgs_do($args)
{

   $delimiter = '@';


   // if the message was not sent, don't log it
   //
   if (!$args[0]) return;


   global $domain, $only_log_domains, $skip_domains,
          $sl_log_events, $sl_log_outgoing_messages_show_from,
          $sl_log_outgoing_messages_show_reply_to,
          $sl_log_outgoing_messages_show_subject,
          $sl_log_outgoing_messages_show_recipients,
          $sl_log_outgoing_messages_show_message_body;
   sl_get_config();


   if (!in_array('OUTGOING_MAIL', $sl_log_events)
    || (isset($only_log_domains['OUTGOING_MAIL']) && is_array($only_log_domains['OUTGOING_MAIL'])
        && !empty($only_log_domains['OUTGOING_MAIL']) && !in_array($domain, $only_log_domains['OUTGOING_MAIL']))
    || (isset($skip_domains['OUTGOING_MAIL']) && is_array($skip_domains['OUTGOING_MAIL'])
        && !empty($skip_domains['OUTGOING_MAIL']) && in_array($domain, $skip_domains['OUTGOING_MAIL'])))
      return;


   // grab TO, CC, and BCC fields from message
   //
   if (check_sm_version(1, 5, 2))
      $message = $args[1];
   else
      $message = $args[2];
   $toList = array();
   $ccList = array();
   $bccList = array();
   $replyToList = array();
   $fromList = array();

   foreach ($message->rfc822_header->to as $recip)
      $toList[] = $recip->mailbox . $delimiter . $recip->host;
   foreach ($message->rfc822_header->cc as $recip)
      $ccList[] = $recip->mailbox . $delimiter . $recip->host;
   foreach ($message->rfc822_header->bcc as $recip)
      $bccList[] = $recip->mailbox . $delimiter . $recip->host;
   foreach ($message->rfc822_header->reply_to as $sender)
      $replyToList[] = $sender->mailbox . $delimiter . $sender->host;
   foreach ($message->rfc822_header->from as $sender)
      $fromList[] = $sender->mailbox . $delimiter . $sender->host;


   // have to remove < and > because otherwise the message ID
   // will get removed entirely by the strip_tags() call in
   // sl_logit()
   //
   if (!empty($message->rfc822_header->message_id))
   {
      $msg = 'Message-ID: ' . str_replace(array('<', '>'), '', $message->rfc822_header->message_id);
      //$msg = 'Message-ID: ' . strtr($message->rfc822_header->message_id, '<>', '()');
   }
   else
   {
      $msg = 'Message-ID: unavailable';
   }


   $address_delim = ', ';


   if ($sl_log_outgoing_messages_show_recipients)
   {
      $msg .= ' (TO: ' . implode($address_delim, $toList) . ')';
      $msg .= ' (CC: ' . implode($address_delim, $ccList) . ')';
      $msg .= ' (BCC: ' . implode($address_delim, $bccList) . ')';
   }

   if ($sl_log_outgoing_messages_show_from)
      $msg .= ' (FROM: ' . implode($address_delim, $fromList) . ')';

   if ($sl_log_outgoing_messages_show_reply_to)
      $msg .= ' (REPLY-TO: ' . implode($address_delim, $replyToList) . ')';

global $body, $subject;
   if ($sl_log_outgoing_messages_show_subject)
      $msg .= ' (SUBJECT: ' . $subject . ')';
      ///$msg .= ' (SUBJECT: ' . $message->subject . ')';

   if ($sl_log_outgoing_messages_show_message_body)
      $msg .= ' (BODY: ' . $body . ')';
      ///$msg .= ' (BODY: ' . $message->body_part . ')';


   sl_logit('OUTGOING_MAIL', $msg);

}



/** 
  * Build log fields and send off to be logged
  *
  * Custom event types are acceptable as long as they are enabled
  * in the configuration file.
  *
  * @param string $event The event type
  * @param string $message The log message - note that any HTML
  *                        tags will be removed (optional; default = empty)
  * @param string $user Override for username (if not given,
  *                     will use the SquirrelMail user found
  *                     in SESSION.  (optional; default = empty)
  *
  */
function sl_logit($event, $message='', $user='')
{

   global $sl_namelookups, $sl_dateformat, $username, $domain;
   sl_get_config();


   // construct log fields
   //
   if (empty($user)) $user = $username;

   $timestamp = time();
   $date_str = sldate($sl_dateformat, $timestamp);

   if (!sqgetGlobalVar('REMOTE_ADDR', $user_address, SQ_SERVER))
      $user_address = '';

   if ($sl_namelookups)
      $user_address .= ' (' . gethostbyaddr($user_address) . ')';


   // remove HTML from message and bring multiline into one line
   //
   $message = strip_tags(preg_replace("/(\r\n|\n|\r)+/", ' ', $message));


   // send off to be logged
   //
   sl_send_to_log($event, $timestamp, $date_str, $user, $domain, $user_address, $message);

}



/** 
  * Dispatch fields to be logged to the registered log types
  *
  * Also sends alerts if needed.
  *
  * Custom event types are acceptable as long as they are enabled
  * in the configuration file.
  *
  * @param string $event The event type
  * @param int $timestamp The date and time of the event
  * @param string $date_str A formatted, humyn-readable version of 
  *                         the $timestamp
  * @param string $user The user that triggered the event (optional; 
  *                     default = empty)
  * @param string $dom The user's domain (optional; default = empty)
  * @param string $remote_addr The user's IP and/or hostname 
  *                            information (optional; default = empty)
  * @param string $message The log message (optional; deafult = empty)
  *
  */
function sl_send_to_log($event, $timestamp, $date_str, $user='', $dom='', $remote_addr='', $message='')
{

   global $sl_logs, $sl_send_alerts, $sl_alert_to, $sl_alert_cc, 
          $sl_alert_bcc, $sl_alert_subject_template;
   sl_get_config();



   // find empty parameters
   //
   if (empty($user)) $user = 'N/A';
   if (empty($dom)) $dom = 'N/A';
   if (empty($remote_addr)) $remote_addr = 'N/A';



   // send message to each log type that is enabled
   //
   foreach ($sl_logs as $log => $allowed_events)
   {
   
      // skip if the current event not logged for any log type
      //
      if (!in_array($event, array_keys($allowed_events))) continue;


      // construct full log message 
      //
      $full_message = str_replace(array('%1', '%2', '%3', '%4', '%5', '%6', '%7'), 
                                  array($event, $user, $dom, $remote_addr, 
                                        $timestamp, $date_str, $message), 
                                  $allowed_events[$event]);


      if ($log == 'FILE')
      {
         sl_log_to_file($full_message);
      }

      else if ($log == 'SQL')
      {
         include_once(SM_PATH . 'plugins/squirrel_logger/database_functions.php');

         // note that in this case, $full_message will just be the revised event name
         sl_log_to_db($full_message, $user, $dom, $remote_addr, $timestamp, $message);
      }

      else if (strpos($log, 'SYSTEM') === 0)
      {
         sl_log_to_system_log($log, $full_message);
      }

   }



   // send alerts if needed
   //
   if (in_array($event, array_keys($sl_send_alerts)))
   {

      // construct full log message 
      //
      $body = str_replace(array('%1', '%2', '%3', '%4', '%5', '%6', '%7'), 
                          array($event, $user, $dom, $remote_addr, 
                                $timestamp, $date_str, $message), 
                          $sl_send_alerts[$event]);


      include_once(SM_PATH . 'plugins/squirrel_logger/alert_functions.php');


      // collect recipients
      //
      if (!empty($sl_alert_to[$event]))
         $to = $sl_alert_to[$event];
      else
         $to = '';

      if (!empty($sl_alert_cc[$event]))
         $cc = $sl_alert_cc[$event];
      else
         $cc = '';

      if (!empty($sl_alert_bcc[$event]))
         $bcc = $sl_alert_bcc[$event];
      else
         $bcc = '';


      // build subject
      //
      $subject = str_replace(array('%1', '%2'), array($event, $user), 
                             $sl_alert_subject_template);


      // send it
      //
      sl_send_alert($body, $subject, $to, $cc, $bcc);

   }

}



/**
  * Log a message to a file
  *
  * @param string $message The log message
  *
  */
function sl_log_to_file($message)
{

   global $sl_logfile;
   sl_get_config();


   // write final message to file
   //
   if ($FILE = @fopen($sl_logfile, 'ab'))
   {
      @fwrite($FILE, $message);
//      @fwrite($FILE, $message, 8192);
      fclose($FILE);
   }

}



/**
  * Log a message to the system log
  *
  * @param string $log     The details that specify the
  *                        log facility, priority, ident,
  *                        options, etc.
  * @param string $message The message to be logged
  *
  */
function sl_log_to_system_log($log, $message)
{

   global $sl_fail_silently, $sl_use_GMT;
   sl_get_config();


   // $log_details will potentially have these elements:
   // "SYSTEM" (fixed), priority, facility, ident, options
   //
   $log_details = explode(':', $log, 5);
   if (empty($log_details[1]))
      $log_details[1] = LOG_INFO;
   else
      eval('$log_details[1] = ' . $log_details[1] . ';'); // change from string to (integer) constant


   // if we have no openlog() details, then don't use it
   //
   if (sizeof($log_details) == 2)
   {
      syslog($log_details[1], $message);
      return;
   }


   // make sure we have default facility
   //
   if (empty($log_details[2]))
      $facility = LOG_SYSLOG;
   else
      eval('$facility = ' . $log_details[2] . ';'); // change from string to (integer) constant


   // make sure we have default ident
   //
   if (empty($log_details[3]))
      $log_details[3] = 0;


   // make sure we have default options
   //
   if (empty($log_details[4]))
      $options = 0;
   else
      eval('$options = ' . $log_details[4] . ';'); // change from string to (integer) constant


   // note that the use of sq_call_function_suppress_errors()
   // requires SM 1.4.12, SM 1.5.2 or Compatibility 2.0.10
   //
   $result = sq_call_function_suppress_errors('openlog', array($log_details[3], $options, $facility));
   if (!$result)
      if ($sl_fail_silently)
         return;
      else
      {

         if (empty($log_details[2])) $log_details[2] = '<not defined>';
         if (empty($log_details[3])) $log_details[3] = '<not defined>';
         if (empty($log_details[4])) $log_details[4] = '<not defined>';
         sl_error('ERROR: cannot open system log facility ' . $log_details[2] . ' with ident ' . $log_details[3] . ' and options ' . $log_details[4]);
         exit;

      }


   // now we can log, but make sure to obey timezone setting
   //
   if ($sl_use_GMT)
   {
      $saved_timezone = getenv('TZ');
      putenv('TZ=UTC');
      sq_call_function_suppress_errors('syslog', array($log_details[1], $message));
      sq_call_function_suppress_errors('closelog');
      putenv('TZ=' . $saved_timezone);
   }
   else
   {
      sq_call_function_suppress_errors('syslog', array($log_details[1], $message));
      sq_call_function_suppress_errors('closelog');
   }

}



/**
  * Print out unexpected error
  *
  * Program execution usually should halt after calling this function
  *
  * @param string $msg The error message
  *
  */
function sl_error($msg)
{

   // don't get caught in a loop of trying to re-report errors of errors!
   //
   global $color, $reporting_error_for_squirrel_logger, $oTemplate;
   $reporting_error_for_squirrel_logger = 1;

   if (check_sm_version (1, 5, 2) && !is_object($oTemplate))
      sl_finish_sm_init();

   $ret = plain_error_message($msg, $color);
   if (check_sm_version (1, 5, 2))
   {
      echo $ret;
      global $oTemplate;
      $oTemplate->display('footer.tpl');
   }

}



/**
  * Finish SquirrelMail initialization tasks just enough to be
  * able to display an error when one occurs before output is
  * otherwise expected.
  *
  */
function sl_finish_sm_init()
{

   if (check_sm_version(1, 5, 2))
   {
      include_once(SM_PATH . 'functions/display_messages.php' );
      include_once(SM_PATH . 'functions/page_header.php');
      include_once(SM_PATH . 'functions/html.php');
      global $sTemplateID, $oTemplate, $squirrelmail_language;
      $sTemplateID = Template::get_default_template_set();
      $oTemplate = Template::construct_template($sTemplateID);
      set_up_language($squirrelmail_language, true);

      return;
   }


return;
//NOTE: The following are copied from another function in the same_ip plugin that does the same thing, except that it appears to need the following because the error message is i18n-ized, whereas in Squirrel Logger, it is not.  However, I am keeping the following just in case I change that fact.


   if (check_sm_version(1, 5, 0))
   {
      include_once(SM_PATH . 'include/load_prefs.php');

      return;
   }


   if (check_sm_version(1, 4, 0))
   {
      // the following line is only needed because webmail.php
      // doesn't include validate.php; argh!
      //
      include_once(SM_PATH . 'functions/i18n.php');


      include_once(SM_PATH . 'functions/display_messages.php');

      return;
   }

}



