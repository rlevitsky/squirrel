<?php

/**
  * SquirrelMail Squirrel Logger Plugin
  *
  * Copyright (c) 2005-2011 Paul Lesniewski <paul@squirrelmail.org>
  * Copyright (c) 2002-2003 Pat Winn <ptwinn@velocibyte.com>
  * Copyright (c) 2001-2004 Ron Chinn <ron@squeaksoft.com>
  *
  * Licensed under the GNU GPL. For full terms see the file COPYING.
  *
  * @package plugins
  * @subpackage squirrel_logger
  *
  */


/**
  * Register this plugin with SquirrelMail
  *
  */
function squirrelmail_plugin_init_squirrel_logger() 
{

   global $squirrelmail_plugin_hooks;

   $squirrelmail_plugin_hooks['login_verified']['squirrel_logger']
      = 'sl_log_login';
   $squirrelmail_plugin_hooks['logout']['squirrel_logger']
      = 'sl_log_logout';
   $squirrelmail_plugin_hooks['logout_error']['squirrel_logger']
      = 'sl_log_login_error';
   $squirrelmail_plugin_hooks['error_box']['squirrel_logger']
      = 'sl_log_error';
   $squirrelmail_plugin_hooks['compose_send']['squirrel_logger']
      = 'sl_monitor_present_msgs';
   $squirrelmail_plugin_hooks['compose_send_after']['squirrel_logger']
      = 'sl_monitor_sent_msgs';
   $squirrelmail_plugin_hooks['configtest']['squirrel_logger']
      = 'squirrel_logger_check_configuration';

}



/**
  * Returns info about this plugin
  *
  */
function squirrel_logger_info()
{

   return array(
                  'english_name' => 'Squirrel Logger',
                  'authors' => array(
                     'Paul Lesniewski' => array(
                        'email' => 'paul@squirrelmail.org',
                        'sm_site_username' => 'pdontthink',
                     ),
                  ),
                  'version' => '2.3.1',
                  'required_sm_version' => '1.2.7',
                  'requires_configuration' => 1,
                  'requires_source_patch' => 0,
                  'summary' => 'Logs user activity to file/database/syslog.',
                  'details' => 'This plugin implements logging functionality for your webmail interface.  You can choose to log to a database, a file, your system log, or any combination thereof.  You can also choose which kinds of events to log, including login events, logout events, login error events, all outgoing messages, possible outgoing spam messages, and other error events.<br /><br />Also included is monitoring functionality that will send alert emails to the administrator when certain events trigger.<br /><br />If you use the timeout_user plugin, logout events caused by user timeouts will be captured.<br /><br />Log message format is also completely custom-defined to meet your needs in the configuration file.',
                  'required_plugins' => array(
                     'compatibility' => array(
                        'version' => '2.0.10',
                        'activate' => FALSE,
                     )
                  )
               );

}



/**
  * Returns version info about this plugin
  *
  */
function squirrel_logger_version()
{

   $info = squirrel_logger_info();
   return $info['version'];

}



/**
  * Log user logins
  *
  */
function sl_log_login() 
{

  include_once(SM_PATH . 'plugins/squirrel_logger/functions.php');
  sl_log_login_do();

}



/**
  * Log user logouts
  *
  */
function sl_log_logout() 
{

  include_once(SM_PATH . 'plugins/squirrel_logger/functions.php');
  sl_log_logout_do();

}



/**
  * Log user login errors
  *
  */
function sl_log_login_error($args) 
{

  include_once(SM_PATH . 'plugins/squirrel_logger/functions.php');
  sl_log_login_error_do($args);

}



/**
  * Log user errors
  *
  */
function sl_log_error($args) 
{

  include_once(SM_PATH . 'plugins/squirrel_logger/functions.php');
  sl_log_error_do($args);

}



/**
  * Monitor mails being sent, watching for possible mass mailings (spam)
  *
  */
function sl_monitor_present_msgs($args) 
{

  include_once(SM_PATH . 'plugins/squirrel_logger/functions.php');
  sl_monitor_present_msgs_do($args);

}



/**
  * Monitor mails when they are sent
  *
  */
function sl_monitor_sent_msgs($args) 
{

  include_once(SM_PATH . 'plugins/squirrel_logger/functions.php');
  sl_monitor_sent_msgs_do($args);

}



/**
  * Validate that this plugin is configured correctly
  *
  * @return boolean Whether or not there was a
  *                 configuration error for this plugin.
  *
  */
function squirrel_logger_check_configuration()
{

   include_once(SM_PATH . 'plugins/squirrel_logger/functions.php');
   return squirrel_logger_check_configuration_do();

}



