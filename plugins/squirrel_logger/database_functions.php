<?php

/**
  * SquirrelMail Squirrel Logger Plugin
  *
  * Copyright (c) 2005-2011 Paul Lesniewski <paul@squirrelmail.org>
  * Copyright (c) 2002-2003 Pat Winn <ptwinn@velocibyte.com>
  * Copyright (c) 2001-2004 Ron Chinn <ron@squeaksoft.com>
  *
  * Licensed under the GNU GPL. For full terms see the file COPYING.
  *
  * @package plugins
  * @subpackage squirrel_logger
  *
  */



//if (!include_once('DB.php'))
if (!@include_once('DB.php'))
{
   echo '<strong><font color="red">DB.php could not be loaded. Please make sure you have Pear installed.  For more details see <a href="http://pear.php.net" target="_blank">http://pear.php.net</a></font></strong>';
   exit;
}



/**
  * Log a message to a database
  *
  * @param string $event The event text
  * @param string $user The user that generated the event
  * @param string $dom The user's domain 
  * @param string $user_address The remote IP and/or host address
  * @param int $timestamp The date/time of the event
  * @param string $message The message to be logged
  *
  */
function sl_log_to_db($event, $user, $dom, $user_address, $timestamp, $message)
{

   global $sl_insert_event_query, $sl_fail_silently;
   sl_get_config();


   $db = sl_get_database_connection();
   if ($sl_fail_silently && $db === FALSE) return;


   $sql = str_replace(array('%1', '%2', '%3', '%4', '%5', '%6'), 
                      array($event, $user, $dom, $user_address, 
                            sldate('Y-m-d H:i:s', $timestamp), 
                            $message), 
                      $sl_insert_event_query);

   
   // send query to database
   //
   $result = $db->query($sql);


   // check for database errors
   //
   if (!$sl_fail_silently && DB::isError($result))
   {

      $msg = $result->getMessage();

      sl_error('ERROR: cannot write to logging database - ' . $msg . ' - ' . $sql);
//      sl_error('ERROR: cannot write to logging database - ' . $msg);
      exit;

   }

}



/**
  * Get a database connection
  *
  * If a connection has already been opened, return that,
  * otherwise, open a new connection.
  *
  * @return object The database connection handle, or FALSE
  *                if a database connection could not be made
  *                (when in silent mode per $sl_fail_silently).
  *
  */
function sl_get_database_connection()
{

   global $sl_db_connection, $sl_dsn, $sl_fail_silently;
   sl_get_config();


   // make a new connection if needed; exit if failure
   //
   if (empty($sl_db_connection))
   {

      $sl_db_connection = DB::connect($sl_dsn);
      if (!$sl_fail_silently && DB::isError($sl_db_connection))
      {

         sl_error('Could not make database connection.');
         exit;

      }

      if (!DB::isError($sl_db_connection))
         $sl_db_connection->setFetchMode(DB_FETCHMODE_ORDERED);
      else
         return FALSE;

   }


   // return connection
   //
   return $sl_db_connection;

}



