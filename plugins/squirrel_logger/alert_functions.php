<?php

/**
  * SquirrelMail Squirrel Logger Plugin
  *
  * Copyright (c) 2005-2011 Paul Lesniewski <paul@squirrelmail.org>
  * Copyright (c) 2002-2003 Pat Winn <ptwinn@velocibyte.com>
  * Copyright (c) 2001-2004 Ron Chinn <ron@squeaksoft.com>
  *
  * Licensed under the GNU GPL. For full terms see the file COPYING.
  *
  * @package plugins
  * @subpackage squirrel_logger
  *
  */


/**
  * Sends email alerts
  *
  * Currently, only text messages are supported; no multipart HTML
  * or attachments allowed.
  *
  * @param $message string Text of message to be sent
  * @param $subject string Short text for inclusion in email subject line
  * @param $send_to string Comma-separated list of destination TO: addresses
  * @param $send_to_cc string Comma-separated list of destination CC: addresses
  * @param $send_to_bcc string Comma-separated list of destination BCC: addresses
  *
  */
function sl_send_alert($message, $subject, $send_to, $send_to_cc, $send_to_bcc)
{

   global $username, $domain, $sl_useSendmail, $sl_smtpServerAddress,
          $sl_smtpPort, $sl_sendmail_path, $sl_sendmail_args,
          $sl_pop_before_smtp, $sl_encode_header_key,
          $sl_smtp_auth_mech, $sl_smtp_sitewide_user,
          $sl_smtp_sitewide_pass, $useSendmail, $smtpServerAddress,
          $smtpPort, $sendmail_path, $sendmail_args, $pop_before_smtp,
          $encode_header_key, $smtp_auth_mech, $smtp_sitewide_user,
          $smtp_sitewide_pass, $sl_alert_from;


   include_once(SM_PATH . 'plugins/squirrel_logger/functions.php');
   sl_get_config();


   // override any SMTP/Sendmail settings...
   //
   $orig_useSendmail = $useSendmail;
   if (!is_null($sl_useSendmail))
      $useSendmail = $sl_useSendmail;
   $orig_smtpServerAddress = $smtpServerAddress;
   if (!is_null($sl_smtpServerAddress))
      $smtpServerAddress = $sl_smtpServerAddress;
   $orig_smtpPort = $smtpPort;
   if (!is_null($sl_smtpPort))
      $smtpPort = $sl_smtpPort;
   $orig_sendmail_path = $sendmail_path;
   if (!is_null($sl_sendmail_path))
      $sendmail_path = $sl_sendmail_path;
   $orig_sendmail_args = $sendmail_args;
   if (!is_null($sl_sendmail_args))
      $sendmail_args = $sl_sendmail_args;
   $orig_pop_before_smtp = $pop_before_smtp;
   if (!is_null($sl_pop_before_smtp))
      $pop_before_smtp = $sl_pop_before_smtp;
   $orig_encode_header_key = $encode_header_key;
   if (!is_null($sl_encode_header_key))
      $encode_header_key = $sl_encode_header_key;
   $orig_smtp_auth_mech = $smtp_auth_mech;
   if (!is_null($sl_smtp_auth_mech))
      $smtp_auth_mech = $sl_smtp_auth_mech;
   $orig_smtp_sitewide_user = $smtp_sitewide_user;
   if (!is_null($sl_smtp_sitewide_user))
      $smtp_sitewide_user = $sl_smtp_sitewide_user;
   $orig_smtp_sitewide_pass = $smtp_sitewide_pass;
   if (!is_null($sl_smtp_sitewide_pass))
      $smtp_sitewide_pass = $sl_smtp_sitewide_pass;


   // set up the From header
   //
   $sl_alert_from = str_replace('%1', $domain, $sl_alert_from);


   // function found in SM core 1.5.2+ and Compatibility plugin 2.0.11+
   // (suppress include error, since file is only needed for 1.5.2+,
   // otherwise Compatibility has us covered)
   //
   @include_once(SM_PATH . 'functions/compose.php');
   $success = sq_send_mail($send_to, $subject, $message, $sl_alert_from, $send_to_cc, $send_to_bcc);


   // return SMTP/Sendmail settings to what they were
   //
   $useSendmail = $orig_useSendmail;
   $smtpServerAddress = $orig_smtpServerAddress;
   $smtpPort = $orig_smtpPort;
   $sendmail_path = $orig_sendmail_path;
   $sendmail_args = $orig_sendmail_args;
   $pop_before_smtp = $orig_pop_before_smtp;
   $encode_header_key = $orig_encode_header_key;
   $smtp_auth_mech = $orig_smtp_auth_mech;
   $smtp_sitewide_user = $orig_smtp_sitewide_user;
   $smtp_sitewide_pass = $orig_smtp_sitewide_pass;

}



