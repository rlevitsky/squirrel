Herein is information about setting up fail2ban (see http://www.fail2ban.org)
to monitor the webmail login failure log entries created by the Squirrel Logger
plugin complements Bill Landry <bill@inetmsg.com>.
===============================================================================


These are basic instruction for setting up fail2ban to monitor the
logfile entries created by SquirrelMail's squirrel_logger plugin.


Add the following to ~/fail2ban/jail.conf (set to http if not using
https, or use "iptables-multiport[port="http,https"]" if using both,
also be sure to set the correct log path and filename for your setup):
===============================================
[squirrelmail-iptables]

enabled  = true
filter   = squirrelmail
action   = iptables[name=SquirrelMail, port=https, protocol=tcp]
          sendmail-whois[name=SquirrelMail, dest=someone@example.com, sender=root@example.com]
logpath  = /var/lib/squirrelmail/prefs/squirrelmail.log
maxretry = 5
bantime  = 3600
===============================================


Create a file called squirrelmail.conf and add the following (this
file needs to be placed in the ~/fail2ban/filter.d subdirectory:
===============================================
# Fail2Ban configuration file
#
# Author: Bill Landry (bill@inetmsg.com)
#
# $Revision: 510 $

[Definition]

# Option: failregex
# Notes.: regex to match the password failures messages in the logfile. The
#         host must be matched by a group named "host". The tag "<HOST>" can
#         be used for standard IP/hostname matching and is only an alias for
#         (?:::f{4,6}:)?(?P<host>\S+)
# Values: TEXT

failregex = \[LOGIN_ERROR\].*from <HOST>: Unknown user or password incorrect

# Option:  ignoreregex
# Notes.:  regex to ignore. If this regex matches, the line is ignored.
# Values:  TEXT

ignoreregex =
===============================================


In order to fail2ban to recognize the date format used in the squirrelmail.log
file, add the following to the ~/fail2ban/server/datedetector.py file:
===============================================
# SquirreMail 09/13/2007 06:43:20
template = DateStrptime()
template.setName("Month/Day/Year Hour:Minute:Second")
template.setRegex("\d{2}/\d{2}/\d{4} \d{2}:\d{2}:\d{2}")
template.setPattern("%m/%d/%Y %H:%M:%S")
self.__templates.append(template)
===============================================

Do a search for "Apache" or "Exim" in the datedetector.py file to find the
section of the file to add the above content.

You can test the regex against your log file using "fail2ban-regex.  For example:

fail2ban-regex /var/lib/squirrelmail/prefs/squirrelmail.log "\[LOGIN_ERROR\].*from <HOST>: Unknown user or password incorrect"

* Be sure to restart the fail2ban daemon after you have completed the configuration.

